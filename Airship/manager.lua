local fuelType = "minecraft:charcoal"
local storageType = "minecraft:barrel"
local fuelIndex = {}
local engineItemAmount = 5

local function getPeripheralList(type)
    local list = {}
    for k, v in pairs(peripheral.getNames()) do
        if(string.find(v, type) ~= nil) then
            table.insert(list, v)
        end
    end

    return list
end

local function IndexFuel()
    fuelIndex = {}
    local all = getPeripheralList(storageType)
    for k, name in pairs(all) do
        local storage = peripheral.wrap(name)
        for slot, item in pairs(storage.list()) do
            if(item.name == fuelType) then
                table.insert(fuelIndex, {["name"] = name, ["slot"] = slot, ["count"] = item.count})
            end
        end

    end
end

local function getStoredFuelLevel()
    IndexFuel()
    local totalCount = 0
    for k, v in pairs(fuelIndex) do
        totalCount = totalCount + v.count
    end
    return totalCount
end

local function deleteEmptyIndex()
    for i = #fuelIndex, 1, -1 do
        if(fuelIndex[i] == 0) then
            table.remove(fuelIndex, i)
        end
    end
end

local function emptyEngines()
    local engines = getPeripheralList("vs_eureka:engine")
    local chests = getPeripheralList(storageType)
    for k, name in pairs(engines) do
        local engine = peripheral.wrap(name)
        if(engine.getItemDetail(1) ~= nil) then
            for k, cName in pairs(chests) do
                if(engine.getItemDetail(1) ~= nil) then
                    engine.pushItems(cName, 1)
                else
                    break
                end
            end
        end
    end
end

local function fuelEngines()
    local engines = getPeripheralList("vs_eureka:engine")

    for k, name in pairs(engines) do
        local engine = peripheral.wrap(name)
        print("Working on engine: " .. name)
        if(engine.getItemDetail(1) == nil or engine.getItemDetail(1).count < engineItemAmount) then
            local count = 0
            if(engine.getItemDetail(1) ~= nil) then count = engine.getItemDetail(1).count end
            local toFill = engineItemAmount - count
            for k, v in pairs(fuelIndex) do
                if(v.count + count > engineItemAmount) then    
                    local extra = v.count + count - engineItemAmount
                    local toMove = v.count - extra
                    print(name .. " extra: " .. extra)
                    fuelIndex[k].count = fuelIndex[k].count - toMove
                    engine.pullItems(v.name, v.slot, toMove)
                elseif(v.count ~= 0) then
                    engine.pullItems(v.name, v.slot)
                end
                if(engine.getItemDetail(1) ~= nil) then count = 0 end
            end
        end
    end
    deleteEmptyIndex()
end

local enableFueling = false
local basalt = require("basalt")
local monitor = peripheral.wrap("monitor_1")
local main = basalt.addMonitor()
local manageThread = main:addThread()
main:setMonitor(monitor)

local fuelLabel = main:addLabel()

local statusLabel = main:addLabel()
statusLabel:setText("Status: Disabled")
statusLabel:setPosition(statusLabel:getParent():getWidth() * 0.5 - statusLabel:getWidth() * 0.5 + 1, 13)


local fuelOnButton = main:addButton():setText("Fuel On")
fuelOnButton:setPosition(fuelOnButton:getParent():getWidth() * 0.5 - fuelOnButton:getWidth() * 0.5 + 1, 3)
fuelOnButton:setBackground(colors.lime)
fuelOnButton:onClick(function(self,event,button,x,y)
    if(event == "mouse_click") then
        print("Button Code: " .. button)
        statusLabel:setText("Status: Enabled")
        statusLabel:setPosition(statusLabel:getParent():getWidth() * 0.5 - statusLabel:getWidth() * 0.5 + 1, 13)
        enableFueling = true
    end
end)

local fuelOffButton = main:addButton():setText("Fuel Off")
fuelOffButton:setPosition(fuelOffButton:getParent():getWidth() * 0.5 - fuelOffButton:getWidth() * 0.5 + 1, 8)
fuelOffButton:setBackground(colors.red)
fuelOffButton:onClick(function(self,event,button,x,y)
    if(event == "mouse_click") then
        statusLabel:setText("Status: Disabled")
        statusLabel:setPosition(statusLabel:getParent():getWidth() * 0.5 - statusLabel:getWidth() * 0.5 + 1, 13)
        enableFueling = false
    end
end)



local function fuelManage()
    while true do
        if(enableFueling) then
            print("Fueling engines")
            fuelEngines()
            print("Done fueling")
        else
            for k, name in pairs(getPeripheralList("vs_eureka:engine")) do
                local engine = peripheral.wrap(name)
                if(engine.getItemDetail(1) ~= nil) then
                    emptyEngines()
                    break
                end
            end
        end
        
        local level = getStoredFuelLevel()
        fuelLabel:setText("Fuel: " .. level)
        fuelLabel:setPosition(fuelLabel:getParent():getWidth() * 0.5 - fuelLabel:getWidth() * 0.5 + 1, 1)
        sleep(10)
    end
end

manageThread:start(fuelManage)
basalt.autoUpdate()
